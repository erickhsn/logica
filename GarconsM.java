import java.util.Scanner;

public class GarconsM
{
	
	public static String imprimeQntd(int qntd, String[] simbolos, int indice)
	{
		String s = "";
		for(int i = 0; i < qntd; i++)
			s += simbolos[indice];
		return s;
	}
	
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		
		String[] simbolos = { "I", "V", "X", "L", "C", "D", "M"};
		
		Scanner sc = new Scanner(System.in);
		String entrada = sc.nextLine();
		int indice = 0;
		String resultado = "";
		for(int i = entrada.length() - 1 ; i >= 0; i--)
		{
			int numero = Integer.parseInt(""+entrada.charAt(i));
			String nova = ""; 
			if(numero > 0 && numero < 4)
				nova = imprimeQntd(numero, simbolos, indice);
			else if (numero == 4)
				nova = simbolos[indice]+""+simbolos[indice+1];
			else if(numero == 5)
				nova = simbolos[indice+1];
			else if(numero > 5 && numero < 9)
			{
				nova = simbolos[indice+1];
				nova += imprimeQntd(numero, simbolos, indice);
			}
			else if(numero == 9)
				nova = simbolos[indice]+""+simbolos[indice+2];
			resultado = nova + resultado;
			indice+= 2;
		}
		System.out.println(resultado);
	}

}
