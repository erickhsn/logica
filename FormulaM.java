import java.util.Scanner;

public class FormulaM 
{
	
	public static int contarRecorrencia(String s, int indice, char c)
	{
		int recorrencias = 0;
		while(indice < s.length())
		{
			if(s.charAt(indice) == c)
			{
				recorrencias++;
				indice++;
			}
			else
			{
				break;
			}
			if(indice + 1 > s.length())break;

		}
		System.out.print(recorrencias + "" + c);
		return indice;
	}
	
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		String entrada = sc.nextLine();
		int i = 0;
		while( i < entrada.length())
			i = contarRecorrencia(entrada, i , entrada.charAt(i));
	}
}
