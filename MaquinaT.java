import java.util.Scanner;

public class MaquinaT
{
	public static boolean isBissexto(int ano)
	{
		//Obs: Fun��o diferente para encontrar numero bissexto, pois as informa��es no git n�o est�o compativeis
		if(ano % 400 == 0)
            return true;
        else if((ano % 4 == 0) && (ano % 100 != 0))
        	return true;
		return false;
	}

	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int dia = sc.nextInt();
		int mes = sc.nextInt();
		int ano = sc.nextInt();
		
		if(mes < 8)
			if(dia >= 1 && dia <= 31 && mes % 2 == 1)
				System.out.println(true);
			else if(dia >= 1 && dia <= 30 && mes % 2 == 0 && mes != 2)
				System.out.println(true);
			else if(dia >= 1 && dia <= 28 && mes == 2 || (dia == 29 && mes == 2 && isBissexto(ano)))
				System.out.println(true);
			else 
				System.out.println(false);
		else if(mes >= 8 && mes <= 12)
			if(dia >= 1 && dia <= 31 && mes % 2 == 0)
				System.out.println(true);
			else if(dia >= 1 && dia <= 30 && mes % 2 == 1)
				System.out.println(true);
			else
				System.out.println(false);
		else
			System.out.println(false);
		
	}

}
